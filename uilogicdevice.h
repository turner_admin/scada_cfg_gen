#ifndef UILOGICDEVICE_H
#define UILOGICDEVICE_H

#include <QWidget>
#include <QString>
#include <QLineEdit>

namespace Ui {
class UiLogicDevice;
}

class UiLogicDevice : public QWidget
{
    Q_OBJECT

public:
    explicit UiLogicDevice(QWidget *parent = 0);
    ~UiLogicDevice();

    QString getName()const;
    QString getDesc()const;

    void setName( const QString& name );
    void setDesc( const QString& desc );

    QString getStatusMapFile( const bool & raw=true )const;
    QString getDiscreteMapFile( const bool& raw=true )const;
    QString getMeasureMapFile( const bool& raw=true )const;
    QString getCumulantMapFile( const bool& raw=true )const;
    QString getRemoteCtlMapFile( const bool& raw=true )const;
    QString getParameterMapFile( const bool& raw=true )const;
    QString getActionMapFile( const bool& raw=true )const;

    void setStatusMapFile( const QString& path );
    void setDiscreteMapFile( const QString& path );
    void setMeasureMapFile( const QString& path );
    void setCumulantMapFile( const QString& path );
    void setRemoteCtlMapFile( const QString& path );
    void setParameterMapFile( const QString& path );
    void setActionMapFile( const QString& path );

    void autoChangeStatusMapFile( const QString& oldFile,const QString& newFile );
    void autoChangeDiscreteMapFile( const QString& oldFile,const QString& newFile );
    void autoChangeMeasureMapFile( const QString& oldFile,const QString& newFile );
    void autoChangeCumulantMapFile( const QString& oldFile,const QString& newFile );
    void autoChangeRemoteCtlMapFile( const QString& oldFile,const QString& newFile );
    void autoChangeParameterMapFile( const QString& oldFile,const QString& newFile );
    void autoChangeActionMapFile( const QString& oldFile,const QString& newFile );

    void setStatusNum( const int& num );
    void setDiscreteNum( const int& num );
    void setMeasureNum( const int& num );
    void setCumulantNum( const int& num );
    void setRemoteCtlNum( const int& num );
    void setParameterNum( const int& num );
    void setActionNum( const int& num );

signals:
    void descChanged( QString desc );
    void statusMapFileChanged( QString oldFile,QString newFile );
    void discreteMapFileChanged( QString oldFile,QString newFile );
    void measureMapFileChanged( QString oldFile,QString newFile );
    void cumulantMapFileChanged( QString oldFile,QString newFile );
    void remoteCtlMapFileChanged( QString oldFile,QString newFile );
    void parameterMapFileChanged( QString oldFile,QString newFile );
    void actionMapFileChanged( QString oldFile,QString newFile );

public slots:
    void onButtonGetStatusMapFile();
    void onButtonGetDiscreteMapFile();
    void onButtonGetMeasureMapFile();
    void onButtonGetCumulantMapFile();
    void onButtonGetRemoteCtlMapFile();
    void onButtonGetParameterMapFile();
    void onButtonGetActionMapFile();

protected:
    void autoChangeMapFile( const QString& oldFile,const QString& newFile,QLineEdit* lineEdit,const char* info );
private:
    Ui::UiLogicDevice *ui;
};

#endif // UILOGICDEVICE_H
