#include "uiscadacfgtool.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    UiScadaCfgTool::ins()->show();

    return a.exec();
}
