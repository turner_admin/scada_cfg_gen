#include "stringlistdb.h"

CStringListDb::CStringListDb()
{
}

int CStringListDb::findOrAdd(const QStringList &descs){
    int n = 0;

    while( true ){
        int s = m_db.indexOf(descs.first(),n);

        if( s<0 ){
            // 新串
            n = m_db.count();
            m_db.append(descs);
            return n;
        }

        n = s;
        for( int i=1;i<descs.count();++i ){
            if( n+i>=m_db.count() ){
                // 已经达到末尾
                m_db.append( descs.mid(i));
                return n;
            }

            if( descs.at(i)!=m_db.at(n+i) ){
                ++n;
                break;
            }
        }

        if( n==s )
            return n;
    }
}
