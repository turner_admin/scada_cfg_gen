#ifndef UISCADACFGTOOL_H
#define UISCADACFGTOOL_H

#include <QWidget>
#include "uideviceinfo.h"
#include "uilogicdevice.h"
#include <QSqlQuery>
#include <QProgressDialog>
#include "stringdb.h"
#include "stringlistdb.h"

namespace Ui {
class UiScadaCfgTool;
}

class UiScadaCfgTool : public QWidget
{
    Q_OBJECT
private:
    explicit UiScadaCfgTool(QWidget *parent = 0);

public:
    static UiScadaCfgTool* ins();

    ~UiScadaCfgTool();

    inline const QString& getFileName()const{
        return m_fileName;
    }

public slots:
    void open();
    void save();
    void saveAs();

    void genAndExport();

    void addDevice();
    void delDevice();
    void delDevice( int index );
    void cloneDevice();

    void help();

    void deviceDescChanged();
    void logicDeviceDescChanged();

    void deviceStatusFileChanged( QString oldFile,QString newFile );
    void deviceDiscreteFileChanged( QString oldFile,QString newFile );
    void deviceMeasureFileChanged( QString oldFile,QString newFile );
    void deviceCumulantFileChanged( QString oldFile,QString newFile );
    void deviceRemoteCtlFileChanged( QString oldFile,QString newFile );
    void deviceParameterFileChanged( QString oldFile,QString newFile );
    void deviceActionFileChanged( QString oldFile,QString newFile );

    void logicDeviceStatusFileChanged( QString oldFile,QString newFile );
    void logicDeviceDiscreteFileChanged( QString oldFile,QString newFile );
    void logicDeviceMeasureFileChanged( QString oldFile,QString newFile );
    void logicDeviceCumulantFileChanged( QString oldFile,QString newFile );
    void logicDeviceRemoteCtlFileChanged( QString oldFile,QString newFile );
    void logicDeviceParameterFileChanged( QString oldFile,QString newFile );
    void logicDeviceActionFileChanged( QString oldFile,QString newFile );

public:
    bool exportCheck();

protected:
    int getFileLines( const QString& path )const;

    bool exportSqlite_createTables( QSqlQuery& query );
    bool exportSqlite_status( const QString& dir,QProgressDialog& pg,int& pg_step,QSqlQuery& query,CStringDb& deviceDb,CStringDb& statusDescDb );
    bool exportSqlite_discrete( const QString& dir,QProgressDialog& pg,int& pg_step,QSqlQuery& query,CStringDb& deviceDb,CStringListDb& discreteDescDb );
    bool exportSqlite_measure( const QString& dir,QProgressDialog& pg,int& pg_step,QSqlQuery& query,CStringDb& deviceDb );
    bool exportSqlite_cumulant( const QString& dir,QProgressDialog& pg,int& pg_step,QSqlQuery& query,CStringDb& deviceDb );
    bool exportSqlite_remoteCtl( const QString& dir,QProgressDialog& pg,int& pg_step,QSqlQuery& query,CStringDb& deviceDb,CStringDb& remoteCtlDescDb );
    bool exportSqlite_parameter( const QString& dir,QProgressDialog& pg,int& pg_step,QSqlQuery& query,CStringDb& deviceDb );
    bool exportSqlite_action( const QString& dir,QProgressDialog& pg,int& pg_step,QSqlQuery& query,CStringDb& deviceDb,CStringDb& actionDescDb );

    bool exportSqlite_device( const QString& dir,QSqlQuery& query );
    bool exportSqlite_statusDesc( const QString& dir,QSqlQuery& query,CStringDb& statusDescDb );
    bool exportSqlite_discreteDesc( const QString& dir,QSqlQuery& query,CStringListDb& discreteDescDb );
    bool exportSqlite_remoteCtlDesc( const QString& dir,QSqlQuery& query,CStringDb& remoteCtlDescDb );
    bool exportSqlite_actionDesc( const QString& dir,QSqlQuery& query,CStringDb& actionDescDb );

    bool exportSqlite_logicStatus( const QString& dir,QProgressDialog& pg,int& pg_step,QSqlQuery& query,CStringDb& logicDeviceDb );
    bool exportSqlite_logicDiscrete( const QString& dir,QProgressDialog& pg,int& pg_step,QSqlQuery& query,CStringDb& logicDeviceDb );
    bool exportSqlite_logicMeasure( const QString& dir,QProgressDialog& pg,int& pg_step,QSqlQuery& query,CStringDb& logicDeviceDb );
    bool exportSqlite_logicCumulant( const QString& dir,QProgressDialog& pg,int& pg_step,QSqlQuery& query,CStringDb& logicDeviceDb );
    bool exportSqlite_logicRemoteCtl( const QString& dir,QProgressDialog& pg,int& pg_step,QSqlQuery& query,CStringDb& logicDeviceDb );
    bool exportSqlite_logicParameter( const QString& dir,QProgressDialog& pg,int& pg_step,QSqlQuery& query,CStringDb& logicDeviceDb );
    bool exportSqlite_logicAction( const QString& dir,QProgressDialog& pg,int& pg_step,QSqlQuery& query,CStringDb& logicDeviceDb );

    bool exportSqlite_logicDevice( const QString& dir,QSqlQuery& query );

private:
    Ui::UiScadaCfgTool *ui;

    QList<UiDeviceInfo*> m_devices;
    QList<UiLogicDevice*> m_logicDevices;

    QString m_fileName;
};

#endif // UISCADACFGTOOL_H
