#-------------------------------------------------
#
# Project created by QtCreator 2017-04-16T23:28:01
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = scada_cfg_gen
TEMPLATE = app


SOURCES += main.cpp\
        uiscadacfgtool.cpp \
    uideviceinfo.cpp \
    stringdb.cpp \
    stringlistdb.cpp \
    uilogicdevice.cpp

HEADERS  += uiscadacfgtool.h \
    uideviceinfo.h \
    stringdb.h \
    stringlistdb.h \
    uilogicdevice.h

FORMS    += uiscadacfgtool.ui \
    uideviceinfo.ui \
    uilogicdevice.ui
