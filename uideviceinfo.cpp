#include "uideviceinfo.h"
#include "ui_uideviceinfo.h"
#include <QFileDialog>
#include <QFileInfo>
#include <QDir>
#include <QString>
#include "uiscadacfgtool.h"
#include <QMessageBox>

extern QString getAbsolutePath( const QString& path,const bool& raw );
extern QString getRelativePath( const QString& path );

UiDeviceInfo::UiDeviceInfo(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::UiDeviceInfo)
{
    ui->setupUi(this);

    connect(ui->lineEdit_desc,SIGNAL(editingFinished()),this,SIGNAL(descChanged()));
}

UiDeviceInfo::~UiDeviceInfo()
{
    delete ui;
}

void UiDeviceInfo::setName(const QString &name){
    ui->lineEdit_name->setText(name);
}

void UiDeviceInfo::setDesc(const QString &name){
    ui->lineEdit_desc->setText(name);
}

void UiDeviceInfo::setStatusCfgFileName(const QString &path){
    ui->lineEdit_status->setText( getRelativePath(path) );
}

void UiDeviceInfo::setDiscreteCfgFileName(const QString &path){
    ui->lineEdit_discrete->setText(getRelativePath(path));
}

void UiDeviceInfo::setMeasureCfgFileName(const QString &path){
    ui->lineEdit_measure->setText(getRelativePath(path));
}

void UiDeviceInfo::setCumulantCfgFileName(const QString &path){
    ui->lineEdit_cumulant->setText(getRelativePath(path));
}

void UiDeviceInfo::setRemoteCtlCfgFileName(const QString &path){
    ui->lineEdit_remoteCtl->setText(getRelativePath(path));
}

void UiDeviceInfo::setParameterCfgFileName(const QString &path){
    ui->lineEdit_paramter->setText(getRelativePath(path));
}

void UiDeviceInfo::setActionCfgFileName(const QString &path){
    ui->lineEdit_action->setText(getRelativePath(path));
}

void UiDeviceInfo::autoChangeStatusCfgFileName(const QString &oldPath, const QString &newPath){
    autoChangeCfgFileName(ui->lineEdit_status,"Status",oldPath,newPath);
}

void UiDeviceInfo::autoChangeDiscreteCfgFileName(const QString &oldPath, const QString &newPath){
    autoChangeCfgFileName(ui->lineEdit_discrete,"Discrete",oldPath,newPath);
}

void UiDeviceInfo::autoChangeMeasureCfgFileName(const QString &oldPath, const QString &newPath){
    autoChangeCfgFileName(ui->lineEdit_measure,"Measure",oldPath,newPath);
}

void UiDeviceInfo::autoChangeCumulantCfgFileName(const QString &oldPath, const QString &newPath){
    autoChangeCfgFileName(ui->lineEdit_cumulant,"Cumulant",oldPath,newPath);
}

void UiDeviceInfo::autoChangeRemoteCtlCfgFileName(const QString &oldPath, const QString &newPath){
    autoChangeCfgFileName(ui->lineEdit_remoteCtl,"Remote Control",oldPath,newPath);
}

void UiDeviceInfo::autoChangeParameterCfgFileName(const QString &oldPath, const QString &newPath){
    autoChangeCfgFileName(ui->lineEdit_paramter,"Parameter",oldPath,newPath);
}

void UiDeviceInfo::autoChangeActionCfgFileName(const QString &oldPath, const QString &newPath){
    autoChangeCfgFileName(ui->lineEdit_action,"Action",oldPath,newPath);
}

QString UiDeviceInfo::getName()const{
    return ui->lineEdit_name->text();
}

QString UiDeviceInfo::getDesc()const{
    return ui->lineEdit_desc->text();
}

QString UiDeviceInfo::getStatusCfgFileName( const bool& raw )const{
    return getAbsolutePath(ui->lineEdit_status->text(),raw );
}

QString UiDeviceInfo::getDiscreteCfgFileName( const bool& raw )const{
    return getAbsolutePath( ui->lineEdit_discrete->text(),raw );
}

QString UiDeviceInfo::getMeasureCfgFileName(const bool& raw )const{
    return getAbsolutePath( ui->lineEdit_measure->text(),raw );
}

QString UiDeviceInfo::getCumulantCfgFileName(const bool& raw )const{
    return getAbsolutePath( ui->lineEdit_cumulant->text(),raw );
}

QString UiDeviceInfo::getRemoteCtlCfgFileName(const bool& raw )const{
    return getAbsolutePath( ui->lineEdit_remoteCtl->text(),raw );
}

QString UiDeviceInfo::getParameterCfgFileName(const bool& raw )const{
    return getAbsolutePath( ui->lineEdit_paramter->text(),raw );
}

QString UiDeviceInfo::getActionCfgFileName(const bool& raw )const{
    return getAbsolutePath( ui->lineEdit_action->text(),raw );
}

void UiDeviceInfo::setStatusNum( const int& num ){
    ui->label_status->setText( QString::number(num));
}

void UiDeviceInfo::setDiscreteNum( const int& num ){
    ui->label_discrete->setText( QString::number(num));
}

void UiDeviceInfo::setMeasureNum( const int& num ){
    ui->label_measure->setText( QString::number(num));
}

void UiDeviceInfo::setCumulantNum( const int& num ){
    ui->label_cumulant->setText( QString::number(num));
}

void UiDeviceInfo::setRemoteCtlNum( const int& num ){
    ui->label_remote->setText( QString::number(num));
}

void UiDeviceInfo::setParameterNum( const int& num ){
    ui->label_parameter->setText( QString::number(num));
}

void UiDeviceInfo::setActionNum(const int &num){
    ui->label_action->setText(QString::number(num));
}

void UiDeviceInfo::getFile_status(){
    QString name = QFileDialog::getOpenFileName(this,tr("Select Cfg File for Statuses"),"",tr("Config File (*.csv)"));
    if( !name.isEmpty() ){
        QString oldName = ui->lineEdit_status->text();

        setStatusCfgFileName(name);

        if( !oldName.isEmpty() && ui->lineEdit_status->text()!=oldName )
            emit( statusFileChanged(oldName,ui->lineEdit_status->text()));
    }
}

void UiDeviceInfo::getFile_discrete(){
    QString name = QFileDialog::getOpenFileName(this,tr("Select Cfg File for Discretes"),"",tr("Config File (*.csv)"));
    if( !name.isEmpty() ){
        QString oldName = ui->lineEdit_discrete->text();
        setDiscreteCfgFileName(name);

        if( !oldName.isEmpty() && ui->lineEdit_discrete->text()!=oldName )
            emit( discreteFileChanged(oldName,ui->lineEdit_discrete->text()));
    }
}

void UiDeviceInfo::getFile_measure(){
    QString name = QFileDialog::getOpenFileName(this,tr("Select Cfg File for Measures"),"",tr("Config File (*.csv)"));
    if( !name.isEmpty() ){
        QString oldName = ui->lineEdit_measure->text();
        setMeasureCfgFileName(name);

        if( !oldName.isEmpty() && ui->lineEdit_measure->text()!=oldName )
            emit( measureFileChanged(oldName,ui->lineEdit_measure->text()));
    }
}

void UiDeviceInfo::getFile_cumulant(){
    QString name = QFileDialog::getOpenFileName(this,tr("Select Cfg File for Cumulantes"),"",tr("Config File (*.csv)"));
    if( !name.isEmpty() ){
        QString oldName = ui->lineEdit_cumulant->text();
        setCumulantCfgFileName(name);

        if( !oldName.isEmpty() && ui->lineEdit_cumulant->text()!=oldName )
            emit( cumulantFileChanged(oldName,ui->lineEdit_cumulant->text()));
    }
}

void UiDeviceInfo::getFile_remoteCtl(){
    QString name = QFileDialog::getOpenFileName(this,tr("Select Cfg File for Remote Controls"),"",tr("Config File (*.csv)"));
    if( !name.isEmpty() ){
        QString oldName = ui->lineEdit_remoteCtl->text();
        setRemoteCtlCfgFileName(name);

        if( !oldName.isEmpty() && ui->lineEdit_remoteCtl->text()!=oldName )
            emit( remoteCtlFileChanged(oldName,ui->lineEdit_remoteCtl->text()));
    }
}

void UiDeviceInfo::getFile_parameter(){
    QString name = QFileDialog::getOpenFileName(this,tr("Select Cfg File for Parameters"),"",tr("Config File (*.csv)"));
    if( !name.isEmpty() ){
        QString oldName = ui->lineEdit_paramter->text();
        setParameterCfgFileName(name);

        if( !oldName.isEmpty() && ui->lineEdit_paramter->text()!=oldName )
            emit( parameterFileChanged(oldName,ui->lineEdit_paramter->text()));
    }
}

void UiDeviceInfo::getFile_action(){
    QString name = QFileDialog::getOpenFileName(this,tr("Select Cfg File for Actions"),"",tr("Config File (*.csv)"));
    if( !name.isEmpty() ){
        QString oldName = ui->lineEdit_action->text();
        setActionCfgFileName(name);

        if( !oldName.isEmpty() && ui->lineEdit_action->text()!=oldName )
            emit( parameterFileChanged(oldName,ui->lineEdit_action->text()));
    }
}

void UiDeviceInfo::autoChangeCfgFileName(QLineEdit *lineEdit, const char* info, const QString &oldPath, const QString &newPath){
    if( oldPath==lineEdit->text() ){
        QMessageBox msgBox(this);
        msgBox.setWindowTitle(QString::fromUtf8("自动修改配置文件"));
        msgBox.setText(QString::fromUtf8("设备 ")+ ui->lineEdit_name->text()+ " "+ QString(info));
        msgBox.setInformativeText( QString(" auto change?\n") + oldPath + QString::fromUtf8("=>") + newPath);
        msgBox.setStandardButtons( QMessageBox::Yes | QMessageBox::No);
        msgBox.setDefaultButton(QMessageBox::Yes);
        if( QMessageBox::Yes== msgBox.exec() ){
            lineEdit->setText(newPath);
        }
    }
}
