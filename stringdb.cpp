#include "stringdb.h"

CStringDb::CStringDb()
{
}

int CStringDb::findOrAdd(const QString &name){
    int i = 0;
    for( QStringList::iterator it=m_db.begin();it!=m_db.end();++it ){
        if( *it==name )
            return i;
        ++i;
    }

    m_db.append(name);
    return i;
}
