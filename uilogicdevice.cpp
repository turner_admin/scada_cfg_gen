﻿#include "uilogicdevice.h"
#include "ui_uilogicdevice.h"

#include <QMessageBox>
#include <QFileDialog>

extern QString getAbsolutePath( const QString& path,const bool& raw );
extern QString getRelativePath( const QString& path );

UiLogicDevice::UiLogicDevice(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::UiLogicDevice)
{
    ui->setupUi(this);

    connect(ui->pushButton_status,SIGNAL(clicked()),this,SLOT(onButtonGetStatusMapFile()));
    connect(ui->pushButton_discrete,SIGNAL(clicked()),this,SLOT(onButtonGetDiscreteMapFile()));
    connect(ui->pushButton_measure,SIGNAL(clicked()),this,SLOT(onButtonGetMeasureMapFile()));
    connect(ui->pushButton_cumulant,SIGNAL(clicked()),this,SLOT(onButtonGetCumulantMapFile()));
    connect(ui->pushButton_remotectl,SIGNAL(clicked()),this,SLOT(onButtonGetRemoteCtlMapFile()));
    connect(ui->pushButton_parameter,SIGNAL(clicked()),this,SLOT(onButtonGetParameterMapFile()));
    connect(ui->pushButton_action,SIGNAL(clicked()),this,SLOT(onButtonGetActionMapFile()));

    connect(ui->lineEdit_desc,SIGNAL(textChanged(QString)),this,SIGNAL(descChanged(QString)));
}

UiLogicDevice::~UiLogicDevice()
{
    delete ui;
}

QString UiLogicDevice::getName()const{
    return ui->lineEdit_name->text();
}

QString UiLogicDevice::getDesc()const{
    return ui->lineEdit_desc->text();
}

void UiLogicDevice::setName(const QString &name){
    ui->lineEdit_name->setText(name);
}

void UiLogicDevice::setDesc(const QString &desc){
    ui->lineEdit_desc->setText(desc);
}

QString UiLogicDevice::getStatusMapFile( const bool & raw )const{
    return getAbsolutePath(ui->lineEdit_status->text(),raw);
}

QString UiLogicDevice::getDiscreteMapFile( const bool& raw )const{
    return getAbsolutePath(ui->lineEdit_discrete->text(),raw);
}

QString UiLogicDevice::getMeasureMapFile( const bool& raw )const{
    return getAbsolutePath(ui->lineEdit_measure->text(),raw);
}

QString UiLogicDevice::getCumulantMapFile( const bool& raw )const{
    return getAbsolutePath(ui->lineEdit_cumulant->text(),raw);
}

QString UiLogicDevice::getRemoteCtlMapFile( const bool& raw )const{
    return getAbsolutePath(ui->lineEdit_remotectl->text(),raw);
}

QString UiLogicDevice::getParameterMapFile( const bool& raw )const{
    return getAbsolutePath(ui->lineEdit_parameter->text(),raw);
}

QString UiLogicDevice::getActionMapFile( const bool& raw )const{
    return getAbsolutePath(ui->lineEdit_action->text(),raw);
}

void UiLogicDevice::setStatusMapFile(const QString &path){
    ui->lineEdit_status->setText(getRelativePath(path));
}

void UiLogicDevice::setDiscreteMapFile(const QString &path){
    ui->lineEdit_discrete->setText(getRelativePath(path));
}

void UiLogicDevice::setMeasureMapFile(const QString &path){
    ui->lineEdit_measure->setText(getRelativePath(path));
}

void UiLogicDevice::setCumulantMapFile(const QString &path){
    ui->lineEdit_cumulant->setText(getRelativePath(path));
}

void UiLogicDevice::setRemoteCtlMapFile(const QString &path){
    ui->lineEdit_remotectl->setText(getRelativePath(path));
}

void UiLogicDevice::setParameterMapFile(const QString &path){
    ui->lineEdit_parameter->setText(getRelativePath(path));
}

void UiLogicDevice::setActionMapFile(const QString &path){
    ui->lineEdit_action->setText(getRelativePath(path));
}

void UiLogicDevice::autoChangeStatusMapFile( const QString& oldFile,const QString& newFile ){
    autoChangeMapFile(oldFile,newFile,ui->lineEdit_status,"Status");
}

void UiLogicDevice::autoChangeDiscreteMapFile( const QString& oldFile,const QString& newFile ){
    autoChangeMapFile(oldFile,newFile,ui->lineEdit_discrete,"Discrete");
}

void UiLogicDevice::autoChangeMeasureMapFile( const QString& oldFile,const QString& newFile ){
    autoChangeMapFile(oldFile,newFile,ui->lineEdit_measure,"Measure");
}

void UiLogicDevice::autoChangeCumulantMapFile( const QString& oldFile,const QString& newFile ){
    autoChangeMapFile(oldFile,newFile,ui->lineEdit_cumulant,"Cumulant");
}

void UiLogicDevice::autoChangeRemoteCtlMapFile( const QString& oldFile,const QString& newFile ){
    autoChangeMapFile(oldFile,newFile,ui->lineEdit_remotectl,"Remote Ctl");
}

void UiLogicDevice::autoChangeParameterMapFile( const QString& oldFile,const QString& newFile ){
    autoChangeMapFile(oldFile,newFile,ui->lineEdit_parameter,"Parameter");
}

void UiLogicDevice::autoChangeActionMapFile( const QString& oldFile,const QString& newFile ){
    autoChangeMapFile(oldFile,newFile,ui->lineEdit_action,"Action");
}

void UiLogicDevice::setStatusNum( const int& num ){
    ui->label_status->setText(QString::number(num));
}

void UiLogicDevice::setDiscreteNum( const int& num ){
    ui->label_discrete->setText(QString::number(num));
}

void UiLogicDevice::setMeasureNum( const int& num ){
    ui->label_measure->setText(QString::number(num));
}

void UiLogicDevice::setCumulantNum( const int& num ){
    ui->label_cumulant->setText(QString::number(num));
}

void UiLogicDevice::setRemoteCtlNum( const int& num ){
    ui->label_remotectl->setText(QString::number(num));
}

void UiLogicDevice::setParameterNum( const int& num ){
    ui->label_parameters->setText(QString::number(num));
}

void UiLogicDevice::setActionNum( const int& num ){
    ui->label_actions->setText(QString::number(num));
}

void UiLogicDevice::onButtonGetStatusMapFile(){
    QString newPath = QFileDialog::getOpenFileName(this,QString::fromUtf8("请选择状态量映射文件")," ","*.csv");
    if( newPath.isEmpty() )
        return;

    newPath = getRelativePath(newPath);

    QString oldPath = ui->lineEdit_status->text();

    if( newPath==oldPath )
        return;

    ui->lineEdit_status->setText(newPath);

    if( !oldPath.isEmpty() )
        emit( statusMapFileChanged(oldPath,newPath));
}

void UiLogicDevice::onButtonGetDiscreteMapFile(){
    QString newPath = QFileDialog::getOpenFileName(this,QString::fromUtf8("请选择离散量映射文件")," ","*.csv");
    if( newPath.isEmpty() )
        return;

    newPath = getRelativePath(newPath);

    QString oldPath = ui->lineEdit_discrete->text();

    if( newPath==oldPath )
        return;

    ui->lineEdit_discrete->setText(newPath);

    if( !oldPath.isEmpty() )
        emit( discreteMapFileChanged(oldPath,newPath));
}

void UiLogicDevice::onButtonGetMeasureMapFile(){
    QString newPath = QFileDialog::getOpenFileName(this,QString::fromUtf8("请选择测量量映射文件")," ","*.csv");
    if( newPath.isEmpty() )
        return;

    newPath = getRelativePath(newPath);

    QString oldPath = ui->lineEdit_measure->text();

    if( newPath==oldPath )
        return;

    ui->lineEdit_measure->setText(newPath);

    if( !oldPath.isEmpty() )
        emit( measureMapFileChanged(oldPath,newPath));
}

void UiLogicDevice::onButtonGetCumulantMapFile(){
    QString newPath = QFileDialog::getOpenFileName(this,QString::fromUtf8("请选择累计量映射文件")," ","*.csv");
    if( newPath.isEmpty() )
        return;

    newPath = getRelativePath(newPath);

    QString oldPath = ui->lineEdit_cumulant->text();

    if( newPath==oldPath )
        return;

    ui->lineEdit_cumulant->setText(newPath);

    if( !oldPath.isEmpty() )
        emit( cumulantMapFileChanged(oldPath,newPath));
}

void UiLogicDevice::onButtonGetRemoteCtlMapFile(){
    QString newPath = QFileDialog::getOpenFileName(this,QString::fromUtf8("请选择远控映射文件")," ","*.csv");
    if( newPath.isEmpty() )
        return;

    newPath = getRelativePath(newPath);

    QString oldPath = ui->lineEdit_remotectl->text();

    if( newPath==oldPath )
        return;

    ui->lineEdit_remotectl->setText(newPath);

    if( !oldPath.isEmpty() )
        emit( remoteCtlMapFileChanged(oldPath,newPath));
}

void UiLogicDevice::onButtonGetParameterMapFile(){
    QString newPath = QFileDialog::getOpenFileName(this,QString::fromUtf8("请选择参数映射文件")," ","*.csv");
    if( newPath.isEmpty() )
        return;

    newPath = getRelativePath(newPath);

    QString oldPath = ui->lineEdit_parameter->text();

    if( newPath==oldPath )
        return;

    ui->lineEdit_parameter->setText(newPath);

    if( !oldPath.isEmpty() )
        emit( parameterMapFileChanged(oldPath,newPath));
}

void UiLogicDevice::onButtonGetActionMapFile(){
    QString newPath = QFileDialog::getOpenFileName(this,QString::fromUtf8("请选择动作映射文件")," ","*.csv");
    if( newPath.isEmpty() )
        return;

    newPath = getRelativePath(newPath);

    QString oldPath = ui->lineEdit_action->text();

    if( newPath==oldPath )
        return;

    ui->lineEdit_action->setText(newPath);

    if( !oldPath.isEmpty() )
        emit( actionMapFileChanged(oldPath,newPath));
}

void UiLogicDevice::autoChangeMapFile( const QString& oldFile,const QString& newFile,QLineEdit* lineEdit,const char* info ){
    if( oldFile==lineEdit->text() ){
        QMessageBox mb(this);
        mb.setWindowTitle(QString::fromUtf8("自动更新映射文件"));
        mb.setText(QString::fromUtf8("逻辑设备")+ui->lineEdit_desc->text()+" "+QString::fromUtf8(info));
        mb.setDetailedText(oldFile+" => "+newFile );
        mb.setStandardButtons( QMessageBox::Yes|QMessageBox::No );
        mb.setDefaultButton(QMessageBox::Yes);
        if( mb.exec()==QMessageBox::Yes ){
            lineEdit->setText(newFile);
        }
    }
}
