#ifndef UIDEVICEINFO_H
#define UIDEVICEINFO_H

#include <QWidget>
#include <QString>
#include <QLineEdit>

namespace Ui {
class UiDeviceInfo;
}

class UiDeviceInfo : public QWidget
{
    Q_OBJECT

public:
    explicit UiDeviceInfo(QWidget *parent = 0);
    ~UiDeviceInfo();

    void setName( const QString& name );
    void setDesc( const QString& desc );

    void setStatusCfgFileName( const QString& path );
    void setDiscreteCfgFileName( const QString& path );
    void setMeasureCfgFileName( const QString& path );
    void setCumulantCfgFileName( const QString& path );
    void setRemoteCtlCfgFileName( const QString& path );
    void setParameterCfgFileName( const QString& path );
    void setActionCfgFileName( const QString& path );

    void autoChangeStatusCfgFileName( const QString& oldPath,const QString& newPath );
    void autoChangeDiscreteCfgFileName( const QString& oldPath,const QString& newPath );
    void autoChangeMeasureCfgFileName( const QString& oldPath,const QString& newPath );
    void autoChangeCumulantCfgFileName( const QString& oldPath,const QString& newPath );
    void autoChangeRemoteCtlCfgFileName( const QString& oldPath,const QString& newPath );
    void autoChangeParameterCfgFileName( const QString& oldPath,const QString& newPath );
    void autoChangeActionCfgFileName( const QString& oldPath,const QString& newPath );

    QString getName()const;
    QString getDesc()const;

    QString getStatusCfgFileName( const bool& raw=false )const;
    QString getDiscreteCfgFileName(  const bool& raw=false )const;
    QString getMeasureCfgFileName(  const bool& raw=false )const;
    QString getCumulantCfgFileName(  const bool& raw=false )const;
    QString getRemoteCtlCfgFileName(  const bool& raw=false )const;
    QString getParameterCfgFileName(  const bool& raw=false )const;
    QString getActionCfgFileName(  const bool& raw=false )const;

    void setStatusNum( const int& num );
    void setDiscreteNum( const int& num );
    void setMeasureNum( const int& num );
    void setCumulantNum( const int& num );
    void setRemoteCtlNum( const int& num );
    void setParameterNum( const int& num );
    void setActionNum( const int& num );

signals:
    void descChanged();

    void statusFileChanged( QString oldFile,QString newFile );
    void discreteFileChanged( QString oldFile,QString newFile );
    void measureFileChanged( QString oldFile,QString newFile );
    void cumulantFileChanged( QString oldFile,QString newFile );
    void remoteCtlFileChanged( QString oldFile,QString newFile );
    void parameterFileChanged( QString oldFile,QString newFile );
    void actionFileChanged( QString oldFile,QString newFile );

public slots:
    void getFile_status();
    void getFile_discrete();
    void getFile_measure();
    void getFile_cumulant();
    void getFile_remoteCtl();
    void getFile_parameter();
    void getFile_action();

protected:
    void autoChangeCfgFileName( QLineEdit* lineEdit,const char* info, const QString& oldPath,const QString& newPath );

private:
    Ui::UiDeviceInfo *ui;
};

#endif // UIDEVICEINFO_H
