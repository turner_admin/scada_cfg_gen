#ifndef STRINGLIST_DB_H
#define STRINGLIST_DB_H

#include <QStringList>

class CStringListDb
{
public:
    CStringListDb();

    int findOrAdd( const QStringList& descs );

    inline const QStringList& getDb()const{
        return m_db;
    }

private:
    QStringList m_db;
};

#endif // STRINGLIST_DB_H
