#ifndef STRING_DB_H
#define STRING_DB_H

#include <QStringList>

class CStringDb
{
public:
    CStringDb();

    int findOrAdd( const QString& name );

    inline const QStringList& getDb()const{
        return m_db;
    }
private:
    QStringList m_db;
};

#endif // STRING_DB_H
